/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Basic;

/**
 *
 * @author DarthInvader
 */
public class Basic {

    /**
     * Sums all the number of the integer array values
     * @param values
     * @return the sum of the number in the values array to a long
     */
    public static long sum(int[] values){
        int sum = 0;
        for(int i:values){
           sum+=i; 
        }
        return sum;
    }
    /**
     * Sums all the number of the float array values
     * @param values
     * @return the sum of the number in the values array to a double
     */
    public static double sum(float[] values){
        double sum = 0;
        for(float i:values){
            sum+=i;
        }
        return sum;
    }
    
    public static long sum(long[] values){
        int sum = 0;
        for(long i:values){
           sum+=i; 
        }
        return sum;
    }
    public static double sum(double[] values){
        double sum = 0;
        for(double i:values){
            sum+=i;
        }
        return sum;
    }
    
    /**
     * Multiplies all the number of the int array values
     * @param values
     * @return the multiplication result of the number in the values array to a long
     */
    public static long multiplication(int[] values){
        long mul = 1;
        for(int i:values){
            mul*=i;
        }
        return mul;
    }
    /**
     * Multiplies all the number of the float array values
     * @param values
     * @return the multiplication result of the number in the values array to a double
     */
    public static double multiplication(float[] values){
        double mul = 1;
        for(float i:values){
            mul*=i;
        }
        return mul;
    }
    
    /**
     *
     * @param values
     * @return
     */
    public static long multiplication(long[] values){
        long mul = 1;
        for(long i:values){
            mul*=i;
        }
        return mul;
    }
    
    public static double multiplication(double[] values){
        double mul = 1;
        for(double i:values){
            mul*=i;
        }
        return mul;
    }
    /**
     * Implementation of power.Works faster than Math.pow. 
     * For integers only.
     * If the second argument is less or equal to 0 then it returns 1
     * If the first argument is 0 then it returns 0
     * 
     * 
     * @param number
     * @param power
     * @return long
     */
    public static long power(int number,int power){
        long powered=1;
        long x = number;
        if(power<0 || number==0){return 0;}
        if(power == 0){return 1;}
        while(power>1){
            if(power%2==0){
                x*=x;
                power/=2;
            }
            else{
                powered*=x;
                x*=x;
                power = (power-1)/2;
           }
        }
        return powered*x;
    }
    
   
}
